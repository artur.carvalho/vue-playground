function hasClass(element: any, className: string) {
  if (element) {
    if (element.classList) return element.classList.contains(className);
    else
      return new RegExp("(^| )" + className + "( |$)", "gi").test(
        element.className
      );
  }

  return false;
}

function find(element: any, selector: any) {
  return isElement(element) ? element.querySelectorAll(selector) : [];
}

function findSingle(element: any, selector: any) {
  return isElement(element) ? element.querySelector(selector) : null;
}

function isElement(obj: any) {
  return typeof HTMLElement === "object"
    ? obj instanceof HTMLElement
    : obj &&
        typeof obj === "object" &&
        obj !== null &&
        obj.nodeType === 1 &&
        typeof obj.nodeName === "string";
}

function resolveFieldData(this: any, data: any, field: any) {
  if (data && Object.keys(data).length && field) {
    if (this.isFunction(field)) {
      return field(data);
    } else if (field.indexOf(".") === -1) {
      return data[field];
    } else {
      const fields = field.split(".");
      let value = data;

      for (let i = 0, len = fields.length; i < len; ++i) {
        if (value == null) {
          return null;
        }

        value = value[fields[i]];
      }

      return value;
    }
  } else {
    return null;
  }
}

export default {
  hasClass,
  find,
  findSingle,
  resolveFieldData,
};
