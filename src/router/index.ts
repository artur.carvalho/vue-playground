import { createRouter, createWebHistory } from "vue-router";
import Home from "../components/HomePage.vue";
import TanTable from "../components/TanTable.vue";
import TreePrime from "../components/trees/TreePrime.vue";
import HeTree from "../components/trees/HeTree.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", component: Home },
    // Should use named routes. that way it's easier to change the url.
    { path: "/tree-prime", name: "tree-prime", component: TreePrime },
    { path: "/tree-he", name: "tree-he", component: HeTree },
    { path: "/ag-grid", component: () => import("../components/AgTable.vue") },
    { path: "/tanstack", component: TanTable },
  ],
});

export default router;
