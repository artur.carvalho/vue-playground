import { faker } from "@faker-js/faker";
import fs from "fs";

console.time("total");
const USERS = [];
const pad = (d) => d.toString().padStart(2, "0");

function formatDate(d) {
  return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join("-");
}

// todo: Make next 2 fields parameters
// Change the next 2 fields
// Blows up at 7M rows, allocation failure; scavenge might not succeed
// FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory
// 5M makes the save blow up
const rowCount = 3_000_000;
const columnCount = 20;

function createRandomUser(idx) {
  const result = {
    // idx1: idx,
    // idx2: idx % 2 === 0 ? "a" : "b",
    name: faker.name.fullName(),
    birthdate: formatDate(faker.date.birthdate()),
    // married: faker.datatype.boolean(),
    // age: faker.datatype.number({ min: 1940, max: 2020 }),
    // description: faker.lorem.sentence(),
    // dog: faker.animal.dog(),
    pressure: faker.finance.amount(),
    email: faker.internet.email(),
  };

  // const currentFieldCount = Object.keys(result).length;
  // for (let index = currentFieldCount + 1; index <= columnCount; index++) {
  //   result[`filler${index}`] = faker.datatype.number(10);
  // }

  return result;
}

for (let i = 1; i <= rowCount; i++) {
  USERS.push(createRandomUser(i));
  // log the progress
  // if (i % 10000 === 0) console.log(i + " rows");
}

const headers = Object.keys(USERS[0]);

// array of objects to array of arrays
const rows = USERS.map((u) => {
  const row = Object.keys(u).map((k) => u[k]);
  return row;
});
const csv = [headers, ...rows];
const text = csv.map((row) => row.join(",")).join("\n");
// console.log(text);
console.log("write to file now");

const colCount = headers.length;
fs.writeFileSync(`generated${rowCount}lx${colCount}c.csv`, text);
console.timeEnd("total");
